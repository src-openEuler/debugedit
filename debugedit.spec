Name: debugedit
Version: 5.1
Release: 4
Summary: Tools for debuginfo creation
License: GPL-2.0-or-later and LGPL-2.1-only and GPL-3.0-only
Group:   Applications
URL: https://sourceware.org/debugedit/
Source0: https://sourceware.org/pub/debugedit/%{version}/%{name}-%{version}.tar.xz
Source1: https://sourceware.org/pub/debugedit/%{version}/%{name}-%{version}.tar.xz.sig

BuildRequires: gcc make
BuildRequires: pkgconfig(libelf)
BuildRequires: pkgconfig(libdw)
BuildRequires: pkgconfig(libxxhash) >= 0.8.0
BuildRequires: /usr/bin/awk
BuildRequires: /usr/bin/dwz
BuildRequires: /usr/bin/help2man
BuildRequires: /usr/bin/ld
BuildRequires: /usr/bin/ln
BuildRequires: /usr/bin/readelf
BuildRequires: /usr/bin/sed

Requires: binutils gawk coreutils xz elfutils findutils
Requires: /usr/bin/gdb-add-index
Suggests: gdb-minimal
Requires: sed dwz grep
Conflicts: rpm < 4.17

Patch1: find-debuginfo.sh-decompress-DWARF-compressed-ELF-se.patch
Patch6001: backport-tests-Ignore-stderr-output-of-readelf-in-debugedit.a.patch
Patch6002: backport-find-debuginfo-Check-files-are-writable-before-modif.patch

Patch9000: add-sw_64-support.patch

%description
Debugedit provides programs and scripts for creating debuginfo and
source file distributions, collect build-ids and rewrite source
paths in DWARF data for debugging, tracing and profiling.

%prep
%autosetup -p1 -n %{name}-%{version}

%build
%configure
%make_build

%install
%make_install
cd %{buildroot}%{_bindir}
ln -s find-debuginfo find-debuginfo.sh

%check
sed -i 's/^\(C\|LD\)FLAGS=.*/\1FLAGS=""/' tests/atlocal
%make_build check

%files
%license COPYING COPYING3 COPYING.LIB
%doc README
%{_bindir}/debugedit
%{_bindir}/sepdebugcrcfix
%{_bindir}/find-debuginfo
%{_bindir}/find-debuginfo.sh
%{_mandir}/man1/debugedit.1*
%{_mandir}/man1/sepdebugcrcfix.1*
%{_mandir}/man1/find-debuginfo.1*

%changelog
* Mon Mar 03 2025 liweigang <liweiganga@uniontech.com> - 5.1-4
- add sw_64 support

* Tue Dec 10 2024 Funda Wang <fundawang@yeah.net> - 5.1-3
- backport upstream patch checking files writable before modifying them

* Thu Nov 28 2024 laokz <zhangkai@iscas.ac.cn> - 5.1-2
- backport upstream patch to avoid tests failure

* Sat Nov 02 2024 Funda Wang <fundawang@yeah.net> - 5.1-1
- update to 5.1
    build id becomes XXH128 rather than SHA1
- drop symlinks in /usr/lib/rpm, it was of no use for years

* Tue May 28 2024 shaojiansong <shaojiansong@kylinos.cn> - 5.0-7
- Fix lack of loongarch64 patch files in src.rpm package which is build from any platform.

* Fri Jan 6 2023 Wenlong Zhang<zhangwenlong@loongson.cn> - 5.0-6
- add loongarch64 support for debugedit

* Mon Nov 14 2022 Wenlong Zhang <zhangwenlong@loongson.cn> - 5.0-5
- Skip some unsupported tests for loongarch

* Tue Nov 08 2022 renhongxun <renhongxun@h-partners.com> 5.0-4
- make it successfully to find debugedit when running /usr/lib/rpm/find-debuginfo.sh

* Fri Oct 21 2022 renhongxun <renhongxun@h-partners.com> 5.0-3
- fix -u option

* Tue Jan 11 2022 renhongxun <renhongxun@huawei.com> 5.0-2
- bugfix

* Sat Dec 25 2021 renhongxun <renhongxun@huawei.com>
- init package
